﻿using Lynk.Models;
using Firebase.Database;
using Firebase.Database.Query;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace Lynk.Services
{

    public class FirebaseHelper
    {
        FirebaseClient firebase = new FirebaseClient("https://lynk-57c46.firebaseio.com/");

        public async Task<System.Collections.Generic.List<Message>> GetAllMessages()
        {

            return (await firebase
              .Child("Messages")
              .OnceAsync<Message>()).Select(item => new Message
              {
                  From = item.Object.From,
                  Id = item.Object.Id
              }).ToList();
        }

        public async Task AddMessage(string Id, string from)
        {

            await firebase
              .Child("Messages")
              .PostAsync(new Message() { Id = Id, From = from });
        }

        public async Task<Message> GetMessage(string Id)
        {
            var allMessages = await GetAllMessages();
            await firebase
              .Child("Messages")
              .OnceAsync<Message>();
            return allMessages.Where(a => a.Id == Id).FirstOrDefault();
        }

        public async Task UpdateMessage(string Id, string from)
        {
            var toUpdateMessage = (await firebase
              .Child("Messages")
              .OnceAsync<Message>()).Where(a => a.Object.Id == Id).FirstOrDefault();

            await firebase
              .Child("Messages")
              .Child(toUpdateMessage.Key)
              .PutAsync(new Message() { Id = Id, From = from });
        }

        public async Task DeleteMessage(string Id)
        {
            var toDeleteMessage = (await firebase
              .Child("Messages")
              .OnceAsync<Message>()).Where(a => a.Object.Id == Id).FirstOrDefault();
            await firebase.Child("Messages").Child(toDeleteMessage.Key).DeleteAsync();

        }
    }

}
