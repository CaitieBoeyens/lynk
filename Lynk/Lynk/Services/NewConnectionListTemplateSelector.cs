﻿using Lynk.Views.Partials;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

using Lynk.Models;

namespace Lynk.Services
{
        class NewConnectionListTemplateSelector : DataTemplateSelector
        {
        DataTemplate ConnectionTemplate;
        DataTemplate NoConnectionTemplate;

        public NewConnectionListTemplateSelector()
        {
            this.ConnectionTemplate = new DataTemplate(typeof(NewConnectionListCell));
            this.NoConnectionTemplate = new DataTemplate(typeof(NoNewConnectionListCell));
        }

            protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
            {
            var connectVm = item as Connection;
            if (connectVm == null)
                return null;


            return (connectVm.Id == "NO_USERS" && String.IsNullOrEmpty(connectVm.Status)) ? NoConnectionTemplate : ConnectionTemplate;
        }

        }
    
}
