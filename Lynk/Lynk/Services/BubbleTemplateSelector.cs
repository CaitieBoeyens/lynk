﻿using Lynk.Models;
using Lynk.Views.Bubbles;
using Xamarin.Forms;

namespace Lynk.Services
{
    class BubbleTemplateSelector : DataTemplateSelector
    {
        DataTemplate incomingDataTemplate;
        DataTemplate outgoingDataTemplate;

        public BubbleTemplateSelector()
        {
            this.incomingDataTemplate = new DataTemplate(typeof(IncomingBubble));
            this.outgoingDataTemplate = new DataTemplate(typeof(OutgoingBubble));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var messageVm = item as Message;
            if (messageVm == null)
                return null;


            return (messageVm.From == App.UserID) ? outgoingDataTemplate : incomingDataTemplate;
        }

    }
}