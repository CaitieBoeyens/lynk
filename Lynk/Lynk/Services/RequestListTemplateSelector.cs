﻿using Lynk.Views.Partials;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

using Lynk.Models;

namespace Lynk.Services
{
    class RequestListTemplateSelector : DataTemplateSelector
    {
        DataTemplate ConnectionTemplate;
        DataTemplate NoConnectionTemplate;

        public RequestListTemplateSelector()
        {
            this.ConnectionTemplate = new DataTemplate(typeof(RequestListCell));
            this.NoConnectionTemplate = new DataTemplate(typeof(NoRequestsListCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var connectVm = item as Connection;
            if (connectVm == null)
                return null;


            return (connectVm.Id == "NO_USERS" && String.IsNullOrEmpty(connectVm.Status)) ? NoConnectionTemplate : ConnectionTemplate;
        }

    }

}


