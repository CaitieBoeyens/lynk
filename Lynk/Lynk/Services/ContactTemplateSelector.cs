﻿using Lynk.Models;
using Lynk.Views.Partials;
using System;
using Xamarin.Forms;

namespace Lynk.Services
{
    class ContactTemplateSelector : DataTemplateSelector
    {
        DataTemplate ContactTemplate;

        public ContactTemplateSelector()
        {
            this.ContactTemplate = new DataTemplate(typeof(ContactListCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
           
            var connectVm = item as Connection;
            if (connectVm == null)
                return null;


            return  ContactTemplate;
        }

    }
}