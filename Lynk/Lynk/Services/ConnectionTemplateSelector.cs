﻿using Lynk.Models;
using Lynk.Views.Partials;
using System;
using Xamarin.Forms;

namespace Lynk.Services
{
    class ConnectionTemplateSelector : DataTemplateSelector
    {
        DataTemplate ConnectionTemplate;
        DataTemplate NoConnectionTemplate;

        public ConnectionTemplateSelector()
        {
            this.ConnectionTemplate = new DataTemplate(typeof(ConnectionListCell));
            this.NoConnectionTemplate = new DataTemplate(typeof(NoConnectionListCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
           
            var connectVm = item as User;
            if (connectVm == null)
                return null;


            return (connectVm.Id == "NO_USERS" && String.IsNullOrEmpty(connectVm.Status)) ? NoConnectionTemplate : ConnectionTemplate;
        }

    }
}