﻿using Lynk.Models;
using Lynk.Views.Partials;
using System;
using Xamarin.Forms;

namespace Lynk.Services
{
    class ChatTemplateSelector : DataTemplateSelector
    {
        DataTemplate ChatTemplate;
        DataTemplate NoChatTemplate;

        public ChatTemplateSelector()
        {
            this.ChatTemplate = new DataTemplate(typeof(ChatListCell));
            this.NoChatTemplate = new DataTemplate(typeof(NoChatListCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
           
            var chatVm = item as Conversation;
            if (chatVm == null)
                return null;


            return (chatVm.With == "NO_CHATS" && String.IsNullOrEmpty(chatVm.User1) && String.IsNullOrEmpty(chatVm.User2)) ? NoChatTemplate : ChatTemplate;
        }

    }
}