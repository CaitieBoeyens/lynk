﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Lynk.Models;
using Lynk.ViewModels;
using System.Linq;

namespace Lynk.Views
{
    public partial class ChatDetailPage : ContentPage
    {
        ChatDetailViewModel viewModel;

        public ChatDetailPage(ChatDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

            ChatDetailViewModel.ScrollToBottom += ScrollToBottom;
        }

        public ChatDetailPage()
        {
            InitializeComponent();

            var item = new Conversation
            {
                User1 = "Chat 1",
                MessagePreview = "This is an item description."
            };

            viewModel = new ChatDetailViewModel(item);
            BindingContext = viewModel;


        }
        protected  override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadMessagesCommand.Execute(null);

            ScrollToBottom();

        }

        private void ScrollToBottom ()
        {
            var lastMessage = this.ChatList.ItemsSource.Cast<object>().LastOrDefault();
            ChatList.ScrollTo(lastMessage, ScrollToPosition.End, true);
        }


    }
}