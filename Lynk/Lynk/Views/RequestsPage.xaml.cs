﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Lynk.ViewModels;
using Lynk.Models;
using Firebase.Database.Query;

namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RequestsPage : ContentPage
    {
        RequestsViewModel viewModel;

        public RequestsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new RequestsViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var connection = args.SelectedItem as Models.Connection;
            if (connection == null)
                return;

            await App.firebase
               .Child("Users").Child(connection.WithId).Child("Connections")
               .PostAsync(new Models.Connection
               {
                   With = App.UserLink,
                   Status = "Available",
               });

            var request = (await App.firebase
                  .Child("Users").Child(connection.WithId).Child("RequestedConnections")
                  .OnceAsync<Models.Connection>()).Where(u => u.Object.With == App.UserLink).FirstOrDefault();

            if (request != null)
            {
                await App.firebase.Child("Users").Child(connection.WithId).Child("RequestedConnections").Child(request.Key).DeleteAsync();
            }

            await App.firebase
                .Child("Users").Child(App.UserID).Child("Connections")
                .PostAsync(new Models.Connection
                {
                    With = connection.With,
                    Status = "Available",
                });


            var requested = (await App.firebase
                  .Child("Users").Child(App.UserID).Child("ConnectionRequests")
                  .OnceAsync<Models.Connection>()).Where(u => u.Object.With == connection.With).FirstOrDefault();

            if (requested != null)
            {
                await App.firebase
                  .Child("Users").Child(App.UserID).Child("ConnectionRequests").Child(requested.Key).DeleteAsync();
            }

            viewModel.LoadConnectionsCommand.Execute(null);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadConnectionsCommand.Execute(null);
        }
    }
}