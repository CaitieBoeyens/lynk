﻿using System;
using System.Linq;

using Xamarin.Forms;

using Lynk.Models;
using Lynk.ViewModels;

using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Auth;
using Xamarin.Essentials;

namespace Lynk.Views
{
    public partial class NewConnectionsPage : ContentPage
    {
        NewConnectionsViewModel viewModel;
        public Conversation Chat { get; set; }

        public NewConnectionsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new NewConnectionsViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var connection = args.SelectedItem as Models.User;
            if (connection == null)
                return;

            await Navigation.PushAsync(new MakeNewConnectionPage(new MakeNewConnectionViewModel(connection), connection));

            // Manually deselect Connection.
            UsersListView.SelectedItem = null;
            viewModel.LoadUsersCommand.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadUsersCommand.Execute(null);
        }

        


        private async void NewConversation_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ContactsPage());
        }

        private void Signout_Clicked(object sender, EventArgs e)
        {
            try
            {
                Application.Current.MainPage = new LoginPage();
            }
            catch (FirebaseAuthException ex)
            {
                Console.WriteLine(ex);

            }
        }

        private async void Generate_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewCodePage());
        }

        private async void requests_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RequestsPage());
        }

        private async void requested_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RequestedPage()); 
        }
    }
}