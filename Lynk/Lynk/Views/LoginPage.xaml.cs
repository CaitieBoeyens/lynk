﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Firebase.Auth;
using Firebase.Database;
using Xamarin.Essentials;

namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Preferences.Set("isLoggedIn", false);
            Preferences.Remove("authUID");
            Preferences.Remove("firebaseToken");
            Console.WriteLine("AUTH_UID: " + Preferences.Get("authUID", ""));
            App.User = null;
            App.UserID = "";
            App.UserLink = "";
            App.authProvider.Dispose();
            App.authProvider = new FirebaseAuthProvider(new FirebaseConfig("AIzaSyCO3CYBxWOJ0vax34uMXk4woY4iXd7NmmU"));
        }

        private async void SignInBtn_Clicked(object sender, EventArgs e)
        {
            string email = emailEntry.Text;
            string password = passwordEntry.Text;



            try
            {
                var auth = await App.authProvider.SignInWithEmailAndPasswordAsync(email, password);




                App.firebase = new FirebaseClient("https://lynk-57c46.firebaseio.com/",
                  new FirebaseOptions
                  {
                      //this is important, and this is where the blog post is wrong
                      AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken)
                  }
                );
                
                App.AuthUID = auth.User.LocalId;

                Preferences.Set("isLoggedIn", "true");
                Preferences.Set("authUID", auth.User.LocalId);
                Preferences.Set("firebaseToken", auth.FirebaseToken);

                try
                {

                    var user = (await App.firebase
                      .Child("Users")
                      .OnceAsync<Models.User>()).Where(u => u.Object.UID == App.AuthUID).First();

                    App.User = user.Object;
                    App.UserID = user.Key;
                    App.UserLink = $"{App.UserID}___{App.User.Name} {App.User.Surname}";


                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);

                }


                Application.Current.MainPage = new NavigationPage(new ChatsPage());

            }
            catch (FirebaseAuthException exception)
            {
                Console.WriteLine("exception:" + exception.Reason);

                switch (exception.Reason)
                {
                    case AuthErrorReason.Undefined:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.UserDisabled:
                        loginError.Text = "Your account has been disabled";
                        break;
                    case AuthErrorReason.UserNotFound:
                        loginError.Text = "Your account cannot be found";
                        break;
                    case AuthErrorReason.InvalidProviderID:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.InvalidAccessToken:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.LoginCredentialsTooOld:
                        loginError.Text = "Your credentials are out of date";
                        break;
                    case AuthErrorReason.MissingRequestURI:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.SystemError:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.InvalidEmailAddress:
                        loginError.Text = "The email address you entered is invalid";
                        break;
                    case AuthErrorReason.MissingPassword:
                        loginError.Text = "Enter your password";
                        break;

                    case AuthErrorReason.MissingEmail:
                        loginError.Text = "Enter your email address";
                        break;
                    case AuthErrorReason.UnknownEmailAddress:
                        loginError.Text = "You need to register an account to login";
                        break;
                    case AuthErrorReason.WrongPassword:
                        loginError.Text = "You entered the incorrect password";
                        break;
                    case AuthErrorReason.MissingRequestType:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.InvalidIDToken:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.MissingIdentifier:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.InvalidIdentifier:
                        loginError.Text = "Something went wrong";
                        break;
                    case AuthErrorReason.AlreadyLinked:
                        loginError.Text = "Something went wrong";
                        break;
                    default:
                        break;
                }



            }

        }

        private void SignUpBtn_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new SignupPage();
        }
    }
}