﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Lynk.Models;
using Lynk.Views;
using Firebase.Database;
using Firebase.Auth;
using Firebase.Database.Query;
using Lynk.ViewModels;
using Xamarin.Essentials;

namespace Lynk.Views
{
    public partial class ChatsPage : ContentPage
    {
        ChatsViewModel viewModel;

        public ChatsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ChatsViewModel();
            NavigationPage.SetTitleIconImageSource(this, "logo_white_space.png");
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var chat = args.SelectedItem as Conversation;
            if (chat == null)
                return;

            await Navigation.PushAsync(new ChatDetailPage(new ChatDetailViewModel(chat)));

            // Manually deselect item.
            ChatsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadChatsCommand.Execute(null);

        }

        private async void NewConnection_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewConnectionsPage());
        }

        private async void NewConversation_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ContactsPage());
        }
        private async void GoLynksBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ContactsPage());
        }

        private void Signout_Clicked(object sender, EventArgs e)
        {
            try
            {
                Application.Current.MainPage = new LoginPage();
            }
            catch (FirebaseAuthException ex)
            {
                Console.WriteLine(ex);

            }
        }
    }
}