﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Firebase.Database;
using Firebase.Database.Query;
using System.Linq;
using System.Threading.Tasks;
using Lynk.Services;
using Lynk.Models;

namespace Lynk.Views

{
    public partial class ExamplePage : ContentPage
    {
        
        public string key;
        public ExamplePage()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //var observable = firebase
            //     .Child("Conversations")
            //     .AsObservable<Conversation>()
            //     .Subscribe(d => Console.WriteLine(d));
            // Console.WriteLine("??????????????????????????????????????");

            var allUsers = await App.firebase
              .Child("Users")
              .OnceAsync<User>();
            Console.WriteLine("allUsers: " + allUsers);

            foreach (var user in allUsers)
            {
                Console.WriteLine("user");
                Console.WriteLine($"{user.Key}: {user.Object.Name}");
            }


            lstMessages.ItemsSource = allUsers;
        }

        private async void BtnAdd_Clicked(object sender, EventArgs e)
        {
            


            txtId.Text = string.Empty;
            txtName.Text = string.Empty;
            await DisplayAlert("Success", "Message Added Successfully", "OK");
            
            
        }

        private async void BtnRetrive_Clicked(object sender, EventArgs e)
        {
            
            //if (Message != null)
            //{
            //    txtId.Text = Message.Id.ToString();
            //    txtName.Text = Message.From;
            //    await DisplayAlert("Success", "Message Retrive Successfully", "OK");

            //}
            //else
            //{
            //    await DisplayAlert("Success", "No Message Available", "OK");
            //}

        }

        private async void BtnUpdate_Clicked(object sender, EventArgs e)
        {
            //await firebaseHelper.UpdateMessage(txtId.Text, txtName.Text);
            //txtId.Text = string.Empty;
            //txtName.Text = string.Empty;
            //await DisplayAlert("Success", "Message Updated Successfully", "OK");
            //var allMessages = await firebaseHelper.GetAllMessages();
            //lstMessages.ItemsSource = allMessages;
        }

        private async void BtnDelete_Clicked(object sender, EventArgs e)
        {
            //await firebaseHelper.DeleteMessage(txtId.Text);
            //await DisplayAlert("Success", "Message Deleted Successfully", "OK");
            //var allMessages = await firebaseHelper.GetAllMessages();
            //lstMessages.ItemsSource = allMessages;
        }
    }
}