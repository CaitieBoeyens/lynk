﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Lynk.Models;
using Firebase.Database.Query;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Lynk.ViewModels;

namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MakeNewConnectionPage : ContentPage
    {
        private User NewConnection { get; set; }
        private MakeNewConnectionViewModel viewModel { get; set; }
        public MakeNewConnectionPage(MakeNewConnectionViewModel viewModel, User connection)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
            NewConnection = connection;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadConnectionsCommand.Execute(null);
            questionHeader.Text = $"Would you like to lynk with {NewConnection.DisplayName}?";
            messageHeader.Text = $"Let {NewConnection.DisplayName} how you know each other, or why you would like to lynk with them.";
        }



        async private void MakeConnection()
        {
            await App.firebase
                .Child("Users").Child(NewConnection.Id).Child("ConnectionRequests")
                .PostAsync(new Models.Connection
                    {
                        With = App.UserLink,
                        Status = "Available",
                        MessagePreview = reasonEntry.Text
                });

            await App.firebase
                .Child("Users").Child(App.UserID).Child("RequestedConnections")
                .PostAsync(new Models.Connection
                     {
                         With = $"{NewConnection.Id}___{NewConnection.DisplayName}",
                         Status = "Available", 
                         MessagePreview = reasonEntry.Text
                });

            reasonEntry.Text = "";
            await Navigation.PopAsync();
        }

        private void ConnectBtn_Clicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(reasonEntry.Text))
            {
                messageError.Text = "";
                MakeConnection();
            } else
            {
                messageError.Text = "Please enter a message";
            }
            
        }
    }
    
}