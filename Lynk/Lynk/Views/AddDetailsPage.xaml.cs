﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Firebase.Auth;
using Firebase.Database;
using Xamarin.Essentials;

using Lynk.Models;
using Firebase.Database.Query;

namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddDetailsPage : ContentPage
    {
        private string connectionLink;

        public AddDetailsPage(string userLink)
        {
            InitializeComponent();

            connectionLink = userLink;

        }

        private async void CreateBtn_Clicked(object sender, EventArgs e)
        {
            string email = emailEntry.Text;
            string password = passwordEntry.Text;
            string passwordConfirm = passwordConfirmEntry.Text;
            string name = firstNameEntry.Text;
            string surname = surnameEntry.Text;

            if (passwordConfirm == password)
            {

                try
                {
                    await App.authProvider.CreateUserWithEmailAndPasswordAsync(email, password);
                    var auth = await App.authProvider.SignInWithEmailAndPasswordAsync(email, password);
                    

                    App.firebase = new FirebaseClient("https://lynk-57c46.firebaseio.com/",
                      new FirebaseOptions
                      {
                          //this is important, and this is where the blog post is wrong
                          AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken)
                      }
                    );


                    await App.firebase
                     .Child("Users")
                     .PostAsync(new Models.User
                     {
                         UID = auth.User.LocalId,
                         Name = name,
                         Surname = surname,
                         DisplayName = $"{name} {surname}",
                         Status = "Available",
                     });

                    App.AuthUID = auth.User.LocalId;

                    Preferences.Set("isLoggedIn", "true");
                    Preferences.Set("authUID", auth.User.LocalId);
                    Preferences.Set("firebaseToken", auth.FirebaseToken);
                    try
                    {

                        var user = (await App.firebase
                          .Child("Users")
                          .OnceAsync<Models.User>()).Where(u => u.Object.UID == App.AuthUID).First();

                        App.User = user.Object;
                        App.UserID = user.Key;
                        App.UserLink = $"{App.UserID}___{App.User.Name} {App.User.Surname}";


                        var sep = connectionLink.IndexOf("___");
                        var id = connectionLink.Substring(0, sep);


                        
                        var connection = (await App.firebase
                          .Child("Users")
                          .OnceAsync<Models.User>()).Where(u => u.Key == id).First();

                        await App.firebase
                         .Child("Users").Child(id).Child("Connections")
                         .PostAsync(new Models.Connection
                         {
                             With = App.UserLink,
                             Status = "Available",
                             ImgUrl = App.User.ImgUrl
                         });

                        await App.firebase
                         .Child("Users").Child(user.Key).Child("Connections")
                         .PostAsync(new Models.Connection
                         {
                             With = connectionLink,
                             Status = "Available",
                             ImgUrl = connection.Object.ImgUrl
                         });
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("EXCEPTION: " + exception);

                    }

                    Application.Current.MainPage = new NavigationPage(new ChatsPage());
                }
                catch (FirebaseAuthException exception)
                {
                    Console.WriteLine(exception.Reason);

                    switch (exception.Reason)
                    {
                        case AuthErrorReason.Undefined:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.UserDisabled:
                            detailsError.Text = "Your account has been disabled";
                            break;
                        case AuthErrorReason.UserNotFound:
                            detailsError.Text = "Your account cannot be found";
                            break;
                        case AuthErrorReason.InvalidProviderID:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.InvalidAccessToken:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.LoginCredentialsTooOld:
                            detailsError.Text = "Your credentials are out of date";
                            break;
                        case AuthErrorReason.MissingRequestURI:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.SystemError:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.InvalidEmailAddress:
                            detailsError.Text = "The email address you entered is invalid";
                            break;
                        case AuthErrorReason.MissingPassword:
                            detailsError.Text = "Enter a password";
                            break;
                        case AuthErrorReason.MissingEmail:
                            detailsError.Text = "Enter your email address";
                            break;
                        case AuthErrorReason.UnknownEmailAddress:
                            detailsError.Text = "You need to register an account to login";
                            break;
                        case AuthErrorReason.WrongPassword:
                            detailsError.Text = "You entered the incorrect password";
                            break;
                        case AuthErrorReason.WeakPassword:
                            detailsError.Text = "Please choose a stronger password";
                            break;
                        case AuthErrorReason.EmailExists:
                            detailsError.Text = "You already have an account. Please log in";
                            break;
                        case AuthErrorReason.MissingRequestType:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.InvalidIDToken:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.MissingIdentifier:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.InvalidIdentifier:
                            detailsError.Text = "Something went wrong";
                            break;
                        case AuthErrorReason.AlreadyLinked:
                            detailsError.Text = "Something went wrong";
                            break;
                        default:
                            break;
                    }

                }
            }
            else
            {
                detailsError.Text = "Password do not match";
                Console.WriteLine("passwords do not match");

            }



        }
    }
}