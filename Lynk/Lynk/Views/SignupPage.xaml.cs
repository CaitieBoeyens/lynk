﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Firebase.Database;
using Firebase.Database.Query;

using Lynk.Models;


namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignupPage : ContentPage
    {
        public SignupPage()
        {
            InitializeComponent();
        }

        private void SignInBtn_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new LoginPage();
        }

        private async void SignUpBtn_Clicked(object sender, EventArgs e)
        {
            try
            {

                var firebase = new FirebaseClient("https://lynk-57c46.firebaseio.com/");

                var code = (await firebase.Child("Codes").OnceAsync<_Code>()).Where(c => c.Object.Code == codeEntry.Text && !c.Object.IsUsed).First();
                Console.WriteLine("code: " + code);

                await firebase.Child("Codes").Child(code.Key).PatchAsync(new _Code { IsUsed = true, UserLink=code.Object.UserLink, Code=code.Object.Code});

                Application.Current.MainPage = new AddDetailsPage(code.Object.UserLink);
            }
            catch (Exception exception)
            {
                codeError.IsVisible = true;
                Console.WriteLine("exception message: " + exception.Message);
            }

        }
    }
}