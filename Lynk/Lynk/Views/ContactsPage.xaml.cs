﻿using System;
using System.Linq;

using Xamarin.Forms;

using Lynk.Models;
using Lynk.ViewModels;

using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Auth;
using Xamarin.Essentials;

namespace Lynk.Views
{
    public partial class ContactsPage : ContentPage
    {
        ConnectionsViewModel viewModel;
        public Conversation Chat { get; set; }

        public ContactsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ConnectionsViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var Connection = args.SelectedItem as Connection;
            if (Connection == null)
                return;

            var hasConversation = false;

           

            try
            {
               var chat = (await App.firebase
                            .Child("Conversations")
                            .OnceAsync<Models.Conversation>())
                            .Where(u => u.Object.User1 == App.UserLink && u.Object.User2 == Connection.With)
                            .First();

                MakeChat(chat);
            }
            catch(Exception ex)
            {
                Console.WriteLine("POST EX: " + ex);
                hasConversation = false;
            }
                

           
            Console.WriteLine( "HAS CONVERSATION: " + hasConversation);
            
        

            if (!hasConversation)
            {
                try
                {
                    await App.firebase
                       .Child("Conversations")
                       .PostAsync(new Conversation
                       {
                           User1 = App.UserLink,
                           User2 = Connection.With,
                           LastUpdated = DateTime.Now.ToString()
                       });

                    var chat = (await App.firebase
                      .Child("Conversations")
                      .OnceAsync<Models.Conversation>()).Where(u => u.Object.User1 == App.UserLink && u.Object.User2 == Connection.With).FirstOrDefault();

                    
                    MakeChat(chat);
                    Console.WriteLine("POST Chat: " + chat);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("POST EX: " + ex); 
                }
            }


            await Navigation.PushAsync(new ChatDetailPage(new ChatDetailViewModel(Chat)));

            // Manually deselect Connection.
            ContactsListView.SelectedItem = null;
        }

        private void MakeChat(FirebaseObject<Conversation> chat)
        {
            var sep1 = chat.Object.User1.IndexOf("___");
            var id1 = chat.Object.User1.Substring(0, sep1);
            var name1 = chat.Object.User1.Substring(sep1 + 3);

            var sep2 = chat.Object.User2.IndexOf("___");
            var id2 = chat.Object.User2.Substring(0, sep2);
            var name2 = chat.Object.User2.Substring(sep2 + 3);


            var with = id1 == App.UserID ? name2 : name1;

            Console.WriteLine("With: " + with);

            Chat = (new Conversation
            {
                Id = chat.Key,
                With = with,
                User1 = chat.Object.User1,
                User2 = chat.Object.User2,
                LastUpdated = chat.Object.LastUpdated

            });
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Connections.Count == 0)
                viewModel.LoadConnectionsCommand.Execute(null);
        }

        private async void NewConnection_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewConnectionsPage());
        }

        private void Signout_Clicked(object sender, EventArgs e)
        {
            try
            {
               
                Application.Current.MainPage = new LoginPage();
            }
            catch (FirebaseAuthException ex)
            {
                Console.WriteLine(ex);

            }
        }
    }
}