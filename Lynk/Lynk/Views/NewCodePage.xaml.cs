﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Lynk.Models;
using Firebase.Database.Query;

namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewCodePage : ContentPage
    {
        List<string> Codes;
        public NewCodePage()
        {
            InitializeComponent();
           
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var newCode = GenerateRandomNo();
            Code.Text = newCode;

            await App.firebase
                    .Child("Codes")
                    .PostAsync(new _Code
                    {
                        UserLink = App.UserLink,
                        Code = newCode
                    });
        }

        public string GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString();
        }
    }
}