﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Lynk.Views.Partials
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoConnectionListCell : ViewCell
    {
        public NoConnectionListCell()
        {
            InitializeComponent();
        }

        private void GoLynksBtn_Clicked(object sender, EventArgs e)
        {

        }
    }
}