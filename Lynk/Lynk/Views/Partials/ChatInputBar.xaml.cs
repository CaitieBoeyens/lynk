﻿using System;
using System.Collections.Generic;
using Lynk.ViewModels;
using Xamarin.Forms;

namespace Lynk.Views.Partials
{
    public partial class ChatInputBar : ContentView
    {
        public ChatInputBar()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.SetBinding(HeightRequestProperty, new Binding("Height", BindingMode.OneWay, null, null, null, chatTextInput));
            }
        }
        public void Handle_Completed(object sender, EventArgs e)
        {
            
        }

        public void UnFocusEntry()
        {
            chatTextInput?.Unfocus();
        }

        private void Send_Clicked(object sender, EventArgs e)
        {
            (this.Parent.Parent.BindingContext as ChatDetailViewModel).OnSendCommand.Execute(chatTextInput.Text);
            chatTextInput.Text = "";
            UnFocusEntry();
        }
    }
}
