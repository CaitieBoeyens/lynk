﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Lynk.Views.Partials
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoRequestedListCell : ViewCell
    {
        public NoRequestedListCell()
        {
            InitializeComponent();
        }
    }
}