﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Lynk.ViewModels;
using Lynk.Models;

namespace Lynk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RequestedPage : ContentPage
    {
        RequestedViewModel viewModel;

        public RequestedPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new RequestedViewModel();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadConnectionsCommand.Execute(null);
        }
    }
}