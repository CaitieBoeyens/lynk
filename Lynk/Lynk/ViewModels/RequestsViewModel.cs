﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Lynk.Models;
using Lynk.Views;
using Lynk.Services;

using Firebase.Database.Query;
using System.Linq;

namespace Lynk.ViewModels
{
    public class RequestsViewModel : BaseViewModel
    {
        public ObservableCollection<Connection> Connections { get; set; }
        public Command LoadConnectionsCommand { get; set; }

        public RequestsViewModel()
        {
            Connections = new ObservableCollection<Connection>();
            LoadConnectionsCommand = new Command(async () => await ExecuteLoadConnectionsCommand());
        }

        async Task ExecuteLoadConnectionsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Connections.Clear();
                var connections = await App.firebase
               .Child("Users")
               .Child(App.UserID).Child("ConnectionRequests").OnceAsync<Models.Connection>();

                foreach (var connection in connections)
                {
                    var sep = connection.Object.With.IndexOf("___");
                    var id = connection.Object.With.Substring(0, sep);
                    var name = connection.Object.With.Substring(sep + 3);

                    Console.WriteLine(connection);
                    Connections.Add(new Connection
                    {
                        With = connection.Object.With,
                        Conversation = connection.Object.Conversation,
                        WithName = name,
                        WithId = id,
                        MessagePreview=connection.Object.MessagePreview,
                        Status = connection.Object.Status
                    });
                }
                if (!Connections.Any())
                {
                    Connections.Add(new Connection
                    {
                        Id = "NO_USERS"
                    });

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}