﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Lynk.Models;

using Firebase.Database;
using Firebase.Database.Query;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Linq;

namespace Lynk.ViewModels
{
    public class ChatDetailViewModel : BaseViewModel
    {
        public Conversation Chat { get; set; }
        public ObservableCollection<Message> Messages { get; set; }
        public Command LoadMessagesCommand { get; set; }
        public Command OnSendCommand { get; set; }

        public delegate void ScrollToBottomAction();
        public static event ScrollToBottomAction ScrollToBottom;

        public string TextToSend { get; set; }

        public string UserID;
        public string UserLink;
        public ChatDetailViewModel(Conversation chat = null)
        {
            Title = chat?.With;
            Chat = chat;
            Messages = new ObservableCollection<Message>();
            LoadMessagesCommand = new Command(async () => await ExecuteLoadMessagesCommand());
            OnSendCommand = new Command(async () => await ExecuteSendMessageCommand(TextToSend));

        }

        async Task ExecuteSendMessageCommand(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                var sep1 = Chat.User1.IndexOf("___");
                var id1 = Chat.User1.Substring(0, sep1);

                var sep2 = Chat.User2.IndexOf("___");
                var id2 = Chat.User2.Substring(0, sep2);


                var to = id1 == App.UserID ? Chat.User2 : Chat.User1;

                Messages.Add(new Message() { Content = message, From = App.UserID, To = to, TimeSent = DateTime.Now, Status="sent" });

                await App.firebase
                    .Child("Conversations").Child(Chat.Id).Child("Messages")
                    .PostAsync(new Message { Content = message, From = App.UserID, To = to, TimeSent = DateTime.Now, Status = "sent" });
                
                 await App.firebase
                    .Child("Conversations").Child(Chat.Id)
                    .PatchAsync(new Conversation { LastUpdated = DateTime.Now.ToString(), MessagePreview=message, User1 = Chat.User1, User2 = Chat.User2 });
                message = string.Empty;

                LoadMessagesCommand.Execute(null);

                ScrollToBottom?.Invoke();
            }

        }

        async Task ExecuteLoadMessagesCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Messages.Clear();


                var messages = (await App.firebase
                  .Child("Conversations").Child(Chat.Id).Child("Messages")
                  .OnceAsync<Models.Message>());

                Console.WriteLine(messages);

                foreach (var message in messages)
                {
                    Messages.Add(message.Object);
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("EX: " + ex);
            }
            finally
            {
                IsBusy = false;
            }

            ScrollToBottom?.Invoke();
        }

        

    }
}
