﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Essentials;

using Lynk.Models;
using Lynk.Views;
using Lynk.Services;

using Firebase.Database.Query;
using System.Linq;

namespace Lynk.ViewModels
{
    public class ChatsViewModel : BaseViewModel
    {

       
        public ObservableCollection<Conversation> Chats { get; set; }
        public Command LoadChatsCommand { get; set; }

        public string UserID;
        public string UserLink;

        public ChatsViewModel()
        {
            Title = "Conversations";
            Chats = new ObservableCollection<Conversation>();
            LoadChatsCommand = new Command(async () => await ExecuteLoadChatsCommand());
            UserID = Preferences.Get("userID", "");
            UserLink= Preferences.Get("userLink", "");

        }

        async Task ExecuteLoadChatsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Chats.Clear();
                var chats = (await App.firebase
                  .Child("Conversations")
                  .OnceAsync<Models.Conversation>()).Where(u => u.Object.User1 == App.UserLink|| u.Object.User2 == App.UserLink);


                if (chats.Any())
                {
                    var orderedChats = chats.OrderByDescending(c => Convert.ToDateTime(c.Object.LastUpdated));

                    foreach (var chat in orderedChats)
                    {
                        Console.WriteLine("DATE" + chat.Object.LastUpdated);

                        var sep1 = chat.Object.User1.IndexOf("___");
                        var id1 = chat.Object.User1.Substring(0, sep1);
                        var name1 = chat.Object.User1.Substring(sep1 + 3);

                        var sep2 = chat.Object.User2.IndexOf("___");
                        var id2 = chat.Object.User2.Substring(0, sep2);
                        var name2 = chat.Object.User2.Substring(sep2 + 3);


                        var with = id1 == App.UserID ? name2 : name1;

                        Console.WriteLine("With: " + with);

                        Chats.Add(new Conversation
                        {
                            Id = chat.Key,
                            With = with,
                            User1 = chat.Object.User1,
                            User2 = chat.Object.User2,
                            MessagePreview = chat.Object.MessagePreview,
                            LastUpdated = chat.Object.LastUpdated

                        });

                    }

                    
                } else
                {
                    Chats.Add(new Conversation
                    {
                        With = "NO_CHATS",
                    });
                }

                

            }
            catch (Exception ex)
            {
                Console.WriteLine("EX: " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}