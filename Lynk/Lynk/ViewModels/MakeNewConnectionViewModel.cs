﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections.ObjectModel;
using Lynk.Models;
using Firebase.Database.Query;
using System.Linq;
using System.Diagnostics;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Lynk.ViewModels
{
    public class MakeNewConnectionViewModel : BaseViewModel
    {

        public ObservableCollection<Connection> MyConnections { get; set; }
        public ObservableCollection<Connection> TheirConnections { get; set; }
        public ObservableCollection<Connection> MutualConnections { get; set; }
        public User NewConnection { get; set; }
        public Command LoadConnectionsCommand { get; set; }
        public MakeNewConnectionViewModel(User connection = null)
        {
            Title = connection?.DisplayName;
            NewConnection = connection;
            MyConnections = new ObservableCollection<Connection>();
            TheirConnections = new ObservableCollection<Connection>();
            MutualConnections = new ObservableCollection<Connection>();
            LoadConnectionsCommand = new Command(async () => await ExecuteLoadConnectionsCommand());

        }

        async Task ExecuteLoadConnectionsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                MyConnections.Clear();
                var connections = await App.firebase
               .Child("Users")
               .Child(App.UserID).Child("Connections").OnceAsync<Models.Connection>();

                foreach (var connection in connections)

                {
                    var sep = connection.Object.With.IndexOf("___");
                    var id = connection.Object.With.Substring(0, sep);
                    var name = connection.Object.With.Substring(sep + 3);


                    MyConnections.Add(new Connection
                    {
                        With = connection.Object.With,
                        Conversation = connection.Object.Conversation,
                        WithName = name,
                        WithId = id,
                        Status = connection.Object.Status
                    });
                } 



                TheirConnections.Clear();
                var thierConnections = await App.firebase
               .Child("Users")
               .Child(NewConnection.Id).Child("Connections").OnceAsync<Models.Connection>();

                foreach (var connection in thierConnections)

                {
                    var sep = connection.Object.With.IndexOf("___");
                    var id = connection.Object.With.Substring(0, sep);
                    var name = connection.Object.With.Substring(sep + 3);


                    TheirConnections.Add(new Connection
                    {
                        With = connection.Object.With,
                        Conversation = connection.Object.Conversation,
                        WithName = name,
                        WithId = id,
                        Status = connection.Object.Status
                    });
                }

                List<string> myConnectionsWith = MyConnections.Select(item => item.With).ToList();
                Console.WriteLine("MY_COUNT" + myConnectionsWith.Count());

                foreach (Connection connection in TheirConnections)
                {
                    if (myConnectionsWith.Contains(connection.With))
                    {
                        MutualConnections.Add(new Connection
                        {
                            With = connection.With,
                            Conversation = connection.Conversation,
                            WithName = connection.WithName,
                            WithId = connection.WithId,
                            Status = connection.Status
                        });
                    }
                }
                Console.WriteLine("COUNT" + MutualConnections.Count());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
