﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Lynk.Models;
using Lynk.Views;
using Lynk.Services;

using Firebase.Database.Query;

namespace Lynk.ViewModels
{
    public class ConnectionsViewModel : BaseViewModel
    {
        public ObservableCollection<Connection> Connections { get; set; }
        public Command LoadConnectionsCommand { get; set; }
       
        public ConnectionsViewModel()
        {
            Title = "My Lynks";
            Connections = new ObservableCollection<Connection>();
            LoadConnectionsCommand = new Command(async () => await ExecuteLoadConnectionsCommand());
        }

        async Task ExecuteLoadConnectionsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            

            try
            {
                Connections.Clear();
                var connections = await App.firebase
               .Child("Users")
               .Child(App.UserID).Child("Connections").OnceAsync<Models.Connection>();

                
                    foreach (var connection in connections)

                    {
                        var sep = connection.Object.With.IndexOf("___");
                        var id = connection.Object.With.Substring(0, sep);
                        var name = connection.Object.With.Substring(sep + 3);


                        Connections.Add(new Connection
                        {
                            With = connection.Object.With,
                            Conversation = connection.Object.Conversation,
                            WithName = name,
                            WithId = id,
                            Status = connection.Object.Status
                        });
                    }
               

                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}