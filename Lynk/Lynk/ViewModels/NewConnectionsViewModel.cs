﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Lynk.Models;
using Lynk.Views;
using Lynk.Services;

using Firebase.Database.Query;
using System.Linq;
using System.Collections.Generic;

namespace Lynk.ViewModels
{
    public class NewConnectionsViewModel : BaseViewModel
    {
        public ObservableCollection<User> Users { get; set; }
        public Command LoadUsersCommand { get; set; }


        public NewConnectionsViewModel()
        {
            Title = "New Lynks";
            Users = new ObservableCollection<User>();
            LoadUsersCommand = new Command(async () => await ExecuteLoadUsersCommand());
        }

        async Task ExecuteLoadUsersCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Users.Clear();
                var users = await App.firebase
               .Child("Users").OnceAsync<Models.User>();

                

                var connections = await App.firebase
                .Child("Users").Child(App.UserID).Child("Connections")
                .OnceAsync<Models.Connection>();

                var connectionRequests = await App.firebase
                    .Child("Users").Child(App.UserID).Child("ConnectionRequests")
                    .OnceAsync<Models.Connection>();

                var Requestedconnections = await App.firebase
                    .Child("Users").Child(App.UserID).Child("RequestedConnections")
                    .OnceAsync<Models.Connection>();

                var ConnectionsList = new List<string>();
                foreach (var connection in connections)
                {
                    var sep = connection.Object.With.IndexOf("___");
                    var id = connection.Object.With.Substring(0, sep);
                    ConnectionsList.Add(id);
                }

                foreach (var connection in connectionRequests)
                {
                    var sep = connection.Object.With.IndexOf("___");
                    var id = connection.Object.With.Substring(0, sep);
                    ConnectionsList.Add(id);
                }

                foreach (var connection in Requestedconnections)
                {
                    var sep = connection.Object.With.IndexOf("___");
                    var id = connection.Object.With.Substring(0, sep);
                    ConnectionsList.Add(id);
                }

                ConnectionsList.Add(App.UserID);

                    foreach (var user in users)
                    {
                    bool connected = ConnectionsList.Contains(user.Key);

                    if (!connected)
                    {
                        Users.Add(new User
                        {
                            Status = user.Object.Status,
                            DisplayName = user.Object.DisplayName,
                            Id = user.Key
                        });
                    }



                }

                if (!Users.Any())
                {
                    Users.Add(new User
                    {
                        Id = "NO_USERS"
                    });

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}