﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynk.Models
{
    class _Code
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string UserLink { get; set; }
        public bool IsUsed { get; set; }
    }
}
