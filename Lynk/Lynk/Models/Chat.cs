﻿using System;

namespace Lynk.Models
{
    public class Chat
    {
        public string Conversation { get; set; }
        public string With { get; set; }
        public string MessagePreview { get; set; }

    }
}