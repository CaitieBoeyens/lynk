﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynk.Models
{
    public class Connection
    {
        public string Id { get; set; }
        public string Conversation { get; set; }
        public string With { get; set; }
        public string WithName { get; set; }
        public string WithId { get; set; }
        public string MessagePreview { get; set; }
        public string Status { get; set; }
        public string ImgUrl { get; set; }

    }
}
