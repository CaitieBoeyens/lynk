﻿using System;

namespace Lynk.Models
{
    public class Message
    {
        public string Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Content { get; set; }
        public DateTime TimeSent { get; set; }
        public DateTime TimeRecieved { get; set; }
        public string Status { get; set; }
    }
}