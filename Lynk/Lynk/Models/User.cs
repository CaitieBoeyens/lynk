﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynk.Models
{
    public class User
    {
        public string Id { get; set; }
        public string UID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ImgUrl { get; set; }
        public string DisplayName { get; set; }
        public string Status { get; set; }
        //public List<Firebase.Database.FirebaseObject<Connection>> Connections { get; set; }
    }
}