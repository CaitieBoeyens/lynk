﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynk.Models
{
    public class Conversation
    {
        public string Id { get; set; }
        public string LastUpdated { get; set; }
        public string MessagePreview { get; set; }
        public string User1 { get; set; }
        public string User2 { get; set; }
        public string With { get; set; }

    }
}
