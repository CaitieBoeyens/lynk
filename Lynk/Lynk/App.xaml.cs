﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Lynk.Services;
using Lynk.Views;
using Lynk.Models;

using System.Threading.Tasks;
using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Auth;
using Xamarin.Essentials;
using System.Linq;

namespace Lynk
{
    public partial class App : Application
    {
        public static Boolean isSignedUp;
        public static Boolean isLoggedIn;

        public static string  UserID;
        public static Models.User  User;
        public static string  AuthUID;
        public static string UserLink;


        public static FirebaseAuthProvider authProvider;

        public static FirebaseClient firebase;

        public App()
        {
            InitializeComponent();

            authProvider = new FirebaseAuthProvider(new FirebaseConfig("AIzaSyCO3CYBxWOJ0vax34uMXk4woY4iXd7NmmU"));

            if (Preferences.Get("authUID", "") == "JwVsbiSWzSbon2LoRy7nIoPVBdn1")
            {
                Preferences.Clear();
            }
            
            isLoggedIn = Preferences.Get("authUID", "") != "";

            Console.WriteLine("AUTH_UID: " + Preferences.Get("authUID", ""));
            Console.WriteLine("USER_ID: " + Preferences.Get("userID", ""));

            if (!isLoggedIn)
            {
                MainPage = new NavigationPage(new LoginPage());

            }
            else
            {
                MainPage = new NavigationPage(new ChatsPage());
            }

        }

        protected async override void OnStart()
        {

            if (isLoggedIn)
            {
                AuthUID = Preferences.Get("authUID", "");

               var firebaseToken = Preferences.Get("firebaseToken", "");
                

                firebase = new FirebaseClient("https://lynk-57c46.firebaseio.com/");

                try
                {

                    var user = (await firebase
                      .Child("Users")
                      .OnceAsync<Models.User>()).Where(u => u.Object.UID == AuthUID).First();

                    User = user.Object;
                    UserID = user.Key;
                    UserLink = $"{UserID}___{User.Name} {User.Surname}";

                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);

                }
            }


        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
